import os
import cv2
import numpy as np  # linear algebra
from keras.models import Sequential
from keras.applications.vgg19 import preprocess_input
from keras.layers import Flatten, Dense, Conv2D, BatchNormalization, MaxPooling2D, Dropout
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer


class Detection:
    def __init__(self):

        self.data = []
        self.labels = []
        self.model = Sequential()
        self.X_train = []
        self.x_test = []
        self.y_train = []
        self.y_test = []
        self.Encoder = LabelBinarizer()

    def read_images(self, fileName):
        dirc = r'C:\Users\1\Desktop\MD\data' + '\\' + fileName
        j = 0
        for imageName in os.listdir(dirc):
            if j > 600:  # how many images to read from data set.
                break
            path = dirc + '\\' + imageName
            img = cv2.imread(path)

            if img is not None:
                img = self.prepareImage(img)
                self.data.append(img)
                self.labels.append(fileName)
            j = j + 1

        # return self.data, self.labels

    def prepareImage(self, img):
        img = cv2.cvtColor(img, cv2.IMREAD_GRAYSCALE)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        img = cv2.resize(img, (100, 100))
        img = img_to_array(img)
        img = preprocess_input(img)

        return img

    def toArray(self):
        self.data = np.array(self.data, dtype="float32")
        self.labels = np.array(self.labels)

    def BinarizeLabels(self):

        self.labels = self.Encoder.fit_transform(self.labels)
        self.labels = to_categorical(self.labels)

    def splitData(self):
        from sklearn.model_selection import train_test_split
        (self.X_train, self.x_test, self.y_train, self.y_test) = train_test_split(self.data, self.labels,
                                                                                  test_size=0.20, stratify=self.labels,
                                                                                  random_state=42)

    def DataPrepare(self):
        self.read_images('with_mask')
        self.read_images('without_mask')
        self.toArray()
        self.BinarizeLabels()
        self.splitData()

    def BuildModel(self):

        self.model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(100, 100, 3)))
        self.model.add(BatchNormalization())
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.2))
        self.model.add(Flatten())
        self.model.add(Dense(2, activation='sigmoid'))
        self.model.summary()

    def train_compile(self):
        self.model.compile(optimizer='adam', loss='binary_crossentropy', metrics='accuracy')
        history = self.model.fit(self.X_train, self.y_train, epochs=20, batch_size=40)  # callbacks=[early_stopping])

    def Evaluate(self):
        from sklearn.metrics import classification_report, confusion_matrix
        from sklearn.metrics import f1_score, precision_score, recall_score, confusion_matrix, accuracy_score

        # print(self.x_test.shape)
        predictions = self.model.predict(self.x_test)
        predictions = np.argmax(predictions, axis=1)
        self.y_test = np.argmax(self.y_test, axis=1)

        # return classification_report(self.y_test, predictions)
        return accuracy_score(self.y_test, predictions)

    def Predict(self, ImagePath):

        img = cv2.imread(ImagePath)
        
        img = self.prepareImage(img)
        img = np.reshape(img, (1, 100, 100, 3))

        predictions = self.model.predict(img)
        predictions = np.argmax(predictions, axis=1)
        # np.array
        return self.Encoder.inverse_transform(np.array(predictions))[0]
