from django.shortcuts import render
from django.http import HttpResponse
from flask import ctx
from keras import models
import pickle
import os
from .Data import Detection
from django.http import FileResponse
import base64

# Create your views here.

'''
def index(request):
    import base64
    with open("media_root/bla.png", "rb") as image_file:
        image_data = base64.b64encode(image_file.read()).decode('utf-8')

    return render(request, 'Detection/showImage.html', {
        "image": image_data
})
def image_post(request):

    print(request.POST.get('text'))
    return render(request, 'Detection/showImage.html', {
})
'''


# view to train & show results for images.
def Train(request):
    d = Detection()
    d.DataPrepare()
    d.BuildModel()
    d.train_compile()
    accuracy_score = d.Evaluate()

    d.model.save("model_saving//model_sequential.h5")
    # save the encoder to output directory
    with open(os.path.join('model_saving//', 'labels'), 'wb') as f:
        pickle.dump(d.Encoder, f)

    print(accuracy_score)
    return HttpResponse(accuracy_score)


# show images from data set.
def showImage(request):
    import base64
    with open("media_root/with_mask/0_0_0 copy 37.jpg", "rb") as image_file:
        image_data = base64.b64encode(image_file.read()).decode('utf-8')

    return render(request, 'Detection/showImage.html', {
        "image": image_data
    })


# fet image & predict outcome
def MakePrediction(request):
    path = "media_root/with_mask/0_0_0 copy 37.jpg"
    d = Detection()
    d.model = models.load_model("model_saving//model_sequential.h5")
    with open('model_saving//labels', 'rb') as pickle_file:
        d.Encoder = pickle.load(pickle_file)
    prediction = d.Predict(path)

    return HttpResponse(prediction)
