from django.urls import path

from . import views

urlpatterns = [
    path('showImage/', views.showImage, name='showImage'),
    path('Train/', views.Train, name='Train'),
    path('MakePrediction/', views.MakePrediction, name='MakePrediction'),

    # path('image_post', views.image_post, name='image_post'),
]
